import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';

import Colors from '../themes/Colors'

const Alert = ({ message, btnText, onPress, show }) => {

  return (
    <>
    {show && (
      <View style={styles.alertContainer}>
        <Text style={styles.alertMessage}>{message}</Text>
        <TouchableOpacity style={styles.alertBtn} onPress={onPress}>
          <Text style={styles.alertBtnText}>{btnText}</Text>
        </TouchableOpacity>
      </View>
    )}
    </>
  )
}

const styles = StyleSheet.create({
  alertContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 6,
    padding: 12,
    borderRadius: 8,
    backgroundColor: Colors.primary,
  },
  alertMessage: {
    fontFamily: 'Poppins',
    fontSize: 13,
    color: Colors.text,
  },
  alertBtn: {
    backgroundColor: Colors.text,
    borderRadius: 8,
  },
  alertBtnText: {
    fontFamily: 'Poppins',
    padding: 6,
    fontSize: 10,
    fontWeight: '700',
    color: Colors.primary,
  },
})
export default Alert;