const getPosterUri = (path) => {
  const base_url = 'http://image.tmdb.org/t/p/w185/'
  return `${base_url}${path}`
}

export { getPosterUri }