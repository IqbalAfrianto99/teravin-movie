import axios from 'axios'

const APIRequestConstructor = () => {
  const BASE_URL = 'https://api.themoviedb.org'


  const config = (token, contentType) => {
    const config = {
      headers: {
        'Content-Type': contentType || 'application/json',
        Authorization: `Bearer ${token}` || ''
      }
    }

    return config
  }

  const errorResponseDefault = {
    status: 400,
    message: 'Terjadi kesalahan'
  }

  const get = async (url, token) => {
    let result
    const configRequest = config(token)

    try {
      const res = await axios.get(`${BASE_URL}${url}`, configRequest)
      result = res.data
    } catch (e) {
      result = e.response?.data || errorResponseDefault
    }

    return result
  }

  const post = async (url, data, token, contentType) => {
    let result
    const configRequest = config(token, contentType)

    try {
      const res = await axios.post(`${BASE_URL}${url}`, data, configRequest)
      result = res.data
    } catch (e) {
      result = e.response?.data || errorResponseDefault
    }

    return result
  }

  const put = async (url, data, token, contentType) => {
    let result
    const configRequest = config(token, contentType)

    try {
      const res = await axios.put(`${BASE_URL}${url}`, data, configRequest)
      result = res.data
    } catch (e) {
      result = e.response?.data || errorResponseDefault
    }

    return result
  }

  const remove = async (url, token, contentType) => {
    let result, res
    const configRequest = config(token, contentType)

    try {
      res = await axios.delete(`${BASE_URL}${url}`, configRequest)
      result = res.data
    } catch (e) {
      result = e.response?.data || errorResponseDefault
    }

    return result
  }

  return { get, post, put, remove }
}

export default APIRequestConstructor
