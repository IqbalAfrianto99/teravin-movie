import API  from '../helpers/api'

const getMovies = async () => {
  const url = '/3/discover/movie?api_key=f7b67d9afdb3c971d4419fa4cb667fbf'

  const res = await API().get(url, null)

  if (res.results) return res.results
  else return false
}

export { getMovies }
