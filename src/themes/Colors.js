const colors = {
  primary: '#42a5f5',
  secondary: '#e8eaf6',
  text: '#fafafa',
};

export default colors;
