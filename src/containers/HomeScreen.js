/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{ useEffect, useState } from 'react';
import {
  ActivityIndicator,
  FlatList,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
  BackHandler
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';

import Colors from '../themes/Colors';
import { getPosterUri } from '../lib/utils'

// Services
import { getMovies } from '../services/discover';

// Components
import Alert from '../components/Alert'

const HomeScreen = () => {
  const [movies, setMovies] = useState([])
  const [alert, setAlert] = useState({
    open: false,
    message: 'Penyimpanan lokal telah diperbaharui',
  })

  const getListMovies = async () => {
    const res = await getMovies()

    if (res) return res
    else return false
  }

  const applyData = async () => {
    setMovies([])
    const data = await AsyncStorage.getItem('movies')

    if (data) {
      setMovies(JSON.parse(data))
      setAlert({ ...alert, open: false })
    }
  }

  const refreshData = async () => {
    const res = await getListMovies()

    if (res) {
      await AsyncStorage.setItem('movies', JSON.stringify(res.slice(0, 10)))

      if (!alert.open) setAlert({...alert, open: true})
    }
  }

  const getStoredMovies = async () => {
    let movies = await AsyncStorage.getItem('movies');

    if (movies) {
      const parsed = JSON.parse(movies)
      setMovies(parsed.slice(0, 10))
    } else {
      movies = await getListMovies()
      if (movies) {
        await AsyncStorage.setItem('movies', JSON.stringify(movies))
        setMovies(movies.slice(0, 10))
      }
    }
  }

  useEffect(() => {
    getStoredMovies()
    // Refresh data every 1 minute
    const interval = setInterval(refreshData, 60 * 1000)

    const backHandler = BackHandler.addEventListener('hardwareBackPress', () => BackHandler.exitApp())

    return () => {
      clearInterval(interval)
      backHandler.remove()
    } //Cleanup
  }, [])

  useEffect(() => {
    if (alert.open) {
      setTimeout(() => setAlert({ ...alert, open: false }), 5 * 1000)
    }
  }, [alert.open])
  const renderItem = ({ item }) => {
    return (
      <View style={styles.itemContainer}>
        <Image
          style={styles.poster}
          source={{
            uri: getPosterUri(item.poster_path)
          }}
        />
        <View style={styles.infoContainer}>
          <Text style={styles.title} numberOfLines={1}>{item.original_title}</Text>
          <View>
            <Text style={styles.date}>
              {moment(item.release_date).format('DD MMMM YYYY')}
            </Text>
          </View>
          <Text style={styles.synopsis} numberOfLines={3}>{item.overview}</Text>
        </View>
      </View>
    )
  }

  return (
    <>
      <StatusBar backgroundColor={Colors.primary} barStyle="dark-content" />
      <SafeAreaView style={styles.root}>
        <View style={styles.listContainer}>
          {movies.length > 0 ? (
            <FlatList
              showsVerticalScrollIndicator={false}
              data={movies}
              renderItem={renderItem}
              keyExtractor={item => item.id.toString()}
            />
          ): (
            <View style={styles.loadingContainer}>
              <ActivityIndicator size="large" color={Colors.primary} />
            </View>
          )}
        </View>
        <Alert show={alert.open} message={alert.message} btnText="Tampilkan" onPress={applyData} />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  poster: {
    width: 75,
    height: 108,
    resizeMode: 'contain',
    borderRadius: 8
  },
  listContainer: {
    flex: 1,
    marginVertical: 8
  },
  loadingContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemContainer: {
    flex: 1,
    flexDirection: 'row',
    marginVertical: 8,
    marginHorizontal: 16,
  },
  infoContainer: {
    flex: 1,
    marginLeft: 8,
  },
  date: {
    fontFamily: 'Poppins',
    fontSize: 12,
    fontWeight: '500',
    color: Colors.primary,
    marginVertical: 4,
  },
  title: {
    fontFamily: 'Poppins',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#4a4a4a',
  },
  synopsis: {
    color: '#666',
    fontSize: 12,
    lineHeight: 20
  }
});

export default HomeScreen;
