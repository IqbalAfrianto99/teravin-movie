/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{ useEffect } from 'react';
import {
  Alert,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';

const HomeScreen = ({navigation}) => {
  const navigateHome = () => navigation.navigate('Home')

  useEffect(() => {
    // setInterval(() => navigation.navigate('Home'), 2000)
    NetInfo.fetch().then(state => {
      if (state.isConnected) navigateHome()
      else {
        Alert.alert(
          'Tidak terhubung',
          'Mohon periksa kembali koneksi internet anda',
          [
            {
              text: 'Ok',
              onPress: () => navigateHome(),
            },
          ]
        );
      }
    })
  }, [])

  return (
    <>
      <SafeAreaView style={styles.root}>
        <View>
          <Text style={styles.title}>Teravin Movie</Text>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#42a5f5',
  },
  title: {
    fontSize: 24,
    fontWeight: '600',
    color: '#fafafa',
  },
});

export default HomeScreen;
